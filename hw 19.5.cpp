﻿#include <iostream>

// создает класс Animal
class Animal {
public:
    // создает Виртуальную функцию Voice
    virtual void Voice() const {
        std::cout << "Some generic animal sound\n";
    }
};

// Класс наследник Dog
class Dog : public Animal {
public:
    // Переопреляет команду Voice для собаки
    void Voice() const override {
        std::cout << "Woof!\n";
    }
};

// Класс наследник Cat
class Cat : public Animal {
public:
    // Переопреляет команду Voice для кошки
    void Voice() const override {
        std::cout << "Meow!\n";
    }
};

// Класс наследник Cow
class Cow : public Animal {
public:
    // Переопреляет команду Voice для коровы
    void Voice() const override {
        std::cout << "Moo!\n";
    }
};

int main() {
    //создает Массив на объекты класса Animal
    const int numAnimals = 3;
    const Animal* animals[numAnimals] = { new Dog(), new Cat(), new Cow() };

    // создает вызов метода Voice для каждого обьекта в массиве
    for (const Animal* animal : animals) {
        animal->Voice();
    }

    // освобождает память
    for (const Animal* animal : animals) {
        delete animal;
    }

    return 0;
}
